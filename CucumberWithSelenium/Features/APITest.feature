#Author: Jasonzhu
Feature: Weatherbit API Test

  Scenario Outline: Successful API call to weatherbit
    Given User sends request with key "<key>" and lat "<lat>" and lon "<lon>"
    Then the reponse is correct with statecode "<statecode>"

    Examples: 
      | key                              | lat       | lon        | statecode |
      | 6f4cea41c701401280790d0589eb4c44 | 40.730610 | -73.935242 | NY        |
