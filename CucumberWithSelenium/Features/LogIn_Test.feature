#Author: Jasonzhu
Feature: NAB call back request

  Scenario Outline: Successful request call back in NAB
    Given User is on NAB Home Loan Page
    When User requests a call back
    When User selects a topic
    When User fills the form with firstname "<firstname>" and lastname "<lastname>" and email "<email>" and phone "<phone>"
    Then User verifies the request is received

    Examples: 
      | firstname | lastname | email                   | phone      |
      | William   | Kim      | william.kim@hotmail.com | 0410110258 |
      | Kumar     | Suresh   | kumar.suresh@gmail.com  | 0418325644 |

      
      