package StepDefinition;


import cucumber.api.java.en.Given;		
import cucumber.api.java.en.Then;		
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;

import org.testng.Assert;

import java.util.ArrayList; 

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class LoginSteps {
	public WebDriver driver;
	public String baseURL = "https://www.nab.com.au/personal/home-loans";
	public int statusCode;
	public String stateCode;
	
	@Given("^User is on NAB Home Loan Page$")
	public void user_is_on_HomePage() throws Throwable 
	{	//initial the driver and navigate to nab home loan page
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.get(baseURL);
		driver.manage().window().maximize();
	}
	
	
	
	@When("^User requests a call back$")
	public void user_requests_a_call_back() throws Throwable 
	{
		// click on request a call back button
		driver.findElement(By.xpath("//*[@class='nabrwd-banner hidden-xs']//p[contains(text(),'Request a call back')]")).click();
	}
	
	@When("^User selects a topic$")
	public void user_selects_a_topic() throws Throwable 
	{
		//get the shadow DOM, select on radio button and click next button
		WebElement newHomeLoanLink =  driver.findElement(By.xpath("//*[@id='contact-form-shadow-root']"));
		getShadowDOMElement(newHomeLoanLink).findElement(By.cssSelector("div[id=myRadioButton-0]")).click();
		WebElement nextButton = getShadowDOMElement(newHomeLoanLink).findElement(By.cssSelector(".Buttonstyle__StyledButton-sc-1vu4swu-3.cchfek > div > span"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", nextButton);
		Thread.sleep(200); 
		nextButton.click();		
	}
			
	@When("^User fills the form with firstname \"(.*)\" and lastname \"(.*)\" and email \"(.*)\" and phone \"(.*)\"$")
	public void user_fills_the_form(String firstName, String lastName, String email, String phone) throws Throwable 
	{
		// close the old window; switch to new window; fill the form and click submit button
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(0));
	    driver.close();
	    driver.switchTo().window(tabs2.get(1));
		driver.findElement(By.xpath("//*[@class='ToggleButton__StyledText-sc-1sk4h07-2 dicaKa' and text()='No']")).click();
		driver.findElement(By.xpath("//*[@id='field-page-Page1-aboutYou-firstName']")).sendKeys(firstName);
		driver.findElement(By.xpath("//*[@id='field-page-Page1-aboutYou-lastName']")).sendKeys(lastName);
		driver.findElement(By.xpath("//*[@class='css-10kdyoj-control react-select__control']")).click();
		driver.switchTo().activeElement().sendKeys(Keys.DOWN);
		driver.switchTo().activeElement().sendKeys(Keys.ENTER);		
		driver.findElement(By.xpath("//*[@id='field-page-Page1-aboutYou-phoneNumber']")).sendKeys(phone);
		driver.findElement(By.xpath("//*[@id='field-page-Page1-aboutYou-email']")).sendKeys(email);
		WebElement submitButton = driver.findElement(By.xpath("//*[@id='page-Page1-btnGroup-submitBtn']/span"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", submitButton);
		Thread.sleep(200); 
		submitButton.click();
	}
	
	@Then("^User verifies the request is received$")
	public void user_verifies_the_request_is_received() throws Throwable 
	{
		// verify  the message displayed in the window
		String succeedMsg = "RECEIVED YOUR REQUEST";
		String returnMessage = driver.findElement(By.xpath("//*[@id='page-outcome-rowthankyoupageheader-columns3-thankyoupageheadercontent2']/h1")).getText();
		Assert.assertTrue(returnMessage.contains(succeedMsg));
		driver.close();
		driver.quit();
	}
	

	public WebElement getShadowDOMElement(WebElement element) {
		// get the shadow web element 
			WebElement ele = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot",element);
			return ele;
	}

	
	@Given("^User sends request with key \"(.*)\" and lat \"(.*)\" and lon \"(.*)\"$")
	public void User_sends_request(String key, String lat, String lon) throws Throwable 
	{
		 RestAssured.baseURI = "http://api.weatherbit.io/v2.0";
		 RequestSpecification httpRequest = RestAssured.given();
		 Response response = httpRequest.queryParam("key", key) 
                 .queryParam("lat", lat) 
                 .queryParam("lon", lon)
                 .get("/current");
		
		 //System.out.println("request " +  httpRequest.toString());
		 statusCode = response.getStatusCode();

		 ResponseBody<?> responsebody = response.getBody();
		 
		 JsonPath jsonPathEvaluator = responsebody.jsonPath();
		 stateCode = jsonPathEvaluator.get("data[0].state_code");

		 System.out.println("status code is " + statusCode);
		 System.out.println("state_code received from Response " + stateCode);
	} 
	
	@Then("^the reponse is correct with statecode \"(.*)\"$")
	public void the_reponse_is_correct(String expectedStateCode) throws Throwable 
	{
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(stateCode, expectedStateCode);
	}
}
